<%--
  Created by IntelliJ IDEA.
  User: phanh
  Date: 7/20/2018
  Time: 11:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Title</title>
<%--my-css--%>
<link rel="stylesheet" href="css/style.css">
<%--common--%>
<jsp:include page="common/style.jsp"></jsp:include>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-6 form-layout">
				<form action="loginAction" method="POST">
					<div class="form-group">
						<label for="username">User name</label> <input type="text"
							class="form-control" name="username" id="username"
							aria-describedby="emailHelp" placeholder="Enter username">
						<small id="emailHelp" class="form-text text-muted">We'll
							never share your username with anyone else. </small>
					</div>
					<div class="form-group">
						<label for="password">Password</label> <input type="password"
							class="form-control" id="password" name="password"
							placeholder="Password">
					</div>
					<div class="form-group form-check">
						<input type="checkbox" class="form-check-input" id="remember"
							name="remember"> <label class="form-check-label"
							for="remember">Remember me</label>
					</div>
					<button type="submit" id="login" class="btn btn-primary">Login</button>
				</form>
			</div>
		</div>
	</div>
	<%--import js--%>
	<jsp:include page="common/js.jsp"></jsp:include>
</body>
</html>
