package jee.action;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jee.services.LoginService;

/**
 * Login Action
 */
@WebServlet(name = "Login Action", urlPatterns = "/loginAction")
public class LoginAction extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/* ======================================== */
	/* ================== DI ================== */
	/* ======================================== */
	protected LoginService loginService;
	/* ======================================== */
	/* =============== CONST ================== */
	/* ======================================== */

	/* ======================================== */
	/* ============ PUBLIC METHOD ============= */
	/* ======================================== */

	/* ======================================== */
	/* ============ PRIVATE METHOD ============ */
	/* ======================================== */

	/* ======================================== */
	/* ========== PROTECTED METHOD ============ */
	/* ======================================== */

	/**
	 * @param request  *dasda*
	 * @param response *dasda*
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		loginService = new LoginService();
		// Get value parameter from JSP
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		try {
			if (loginService.getAccountLogin(username, password)) {
				System.out.println("Login success.");
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("/listUserAction");
				request.setAttribute("uname", username);
				request.setAttribute("pass", password);
				requestDispatcher.forward(request, response);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
