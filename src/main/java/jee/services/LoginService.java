package jee.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Logger;

import jee.utils.ConnectionUtils;

public class LoginService {

	/*
	 * =============================================================================
	 * ========================= DI ================================================
	 */

	protected Logger logger;

	/*
	 * =============================================================================
	 * ========================= CONST =============================================
	 */

	/*
	 * =============================================================================
	 * ========================= PUBLIC METHOD =====================================
	 * 
	 */
	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws SQLException
	 */
	public boolean getAccountLogin(String username, String password) throws SQLException {
		// Create object connection
		Connection connection = ConnectionUtils.getConnection();
		// Create SQL
		String getAllAccount = "SELECT\n" + "  USERNAME,\n" + "  PASSWORD\n" + "FROM USERS\n"
				+ "WHERE USERNAME = ? and PASSWORD = ?";
		// Create object prepareStatement execute SQL
		PreparedStatement preparedStatement = connection.prepareStatement(getAllAccount);
		preparedStatement.setString(1, username);
		preparedStatement.setString(2, password);

		return preparedStatement.execute();
	}

	/*
	 * ==========================================================================
	 * ========================= PRIVATE METHOD =================================
	 */

}
