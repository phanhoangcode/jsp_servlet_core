package jee.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtils {

    private static final String URL = "jdbc:oracle:thin:@//localhost:1521/orcl12c";
    private static final String USERNAME = "hoangdp";
    private static final String PASSWORD = "hoang24893";

    // Test connection
    public static void main(String[] args) {
        // Test connection
        if (getConnection() != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }
    }

    //Get connection
    public static Connection getConnection() {
        try {
            System.out.println("== Oracle connection JDBC testing ==");
            // Load driver class
            Class.forName("oracle.jdbc.driver.OracleDriver");

        } catch (ClassNotFoundException e) {
            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();
        }
        System.out.println("Oracle JDBC Driver Registered!");
        Connection connection = null;

        try {
            // Create connection object
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
        }
        return connection;
    }
}
